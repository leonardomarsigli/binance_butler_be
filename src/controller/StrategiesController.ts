import { Request } from "express"
import { Strategy } from "../entity/Strategy";
import { IProposedStrategy } from "../utilities/strategyBuilder";
import { StrategyBuilder } from "../utilities/strategyBuilder/builder";

interface AllRequest extends Request {
  body: {
    amount: number,
    currency: 'EUR' | 'USD',
    days: number,
  }
}

export class StrategiesController {
  strategyBuilder: StrategyBuilder;

  async all({ body }: AllRequest): Promise<IProposedStrategy[]> {
    const { amount, currency, days } = body;
    const strategies = await Strategy.find();
    const proposedStrategies: IProposedStrategy[] = [];
    
    for (const strategy of strategies) {
      this.strategyBuilder = new StrategyBuilder({
        amount,
        days,
        strategy,
        currency,
      });
      
      const proposedStrategy = await this.strategyBuilder.calculateStrategy();
      proposedStrategies.push(proposedStrategy);
    }

    return proposedStrategies;
  }
}