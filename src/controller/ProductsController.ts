import { Request } from "express"
import { ProductMap } from "../utilities/scraper/interfaces";
import * as _ from 'lodash';

interface AllRequest extends Request {
  body: {
    currencies: string[],
  }
}

export class ProductsController {
  async all({ body: { currencies } }: AllRequest): Promise<ProductMap> {
    const _availabilities = require('../utilities/scraper/availabilities.json')
    return _.pick(_availabilities, currencies);
  }
}