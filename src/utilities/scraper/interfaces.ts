interface AvailableProducts {
  fixedSavings?: {
    days: string,
    APY: string,
  }[],
  lockedStaking?: {
    days: string,
    APY: string,
  }[],
}

interface Config {
  productTypes: string[];
  cache: {
    lastUpdated: number;
  }
}

export type Availabilities = {
  config: Config;
} & {
  [key: string]: AvailableProducts;
};

export interface ProductMap {
  [key: string]: AvailableProducts;
}