import * as _ from 'lodash';
import puppeteer = require('puppeteer')
import path = require('path')
import { writeFile } from 'fs';
import { Availabilities, ProductMap } from './interfaces';

const BINANCE_ENDPOINT = 'https://www.binance.com/'
const LOCALE = '/en'
const EARN_ENDPOINT = '/earn'
const AVOIDABLES = ['image', 'stylesheet', 'font'];

let _browser: puppeteer.Browser;
let _availabilities: Availabilities = require('./availabilities.json');

async function setUp(): Promise<any> {
  _browser = _browser || await puppeteer.launch({
    executablePath: '/usr/bin/google-chrome',
    args: ['--no-sandbox'],
    timeout: 0,
  });
  _availabilities = require('./availabilities.json');
}

async function createPage(): Promise<puppeteer.Page> {
  const page = await _browser.newPage()
  page.setDefaultNavigationTimeout(0);
  await page.setViewport(({ width: 800, height: 200 }))
  await page.setRequestInterception(true)
  page.on('request', (request) => {
    if (AVOIDABLES.includes(request.resourceType())) request.abort()
    else request.continue()
  });
  return page
}


async function crawlPage(currency: string): Promise<void> {
  const url = new URL(path.join(BINANCE_ENDPOINT, LOCALE, EARN_ENDPOINT, currency.toLowerCase()))
  const page = await createPage()
  await page.goto(url.href)

  const evaluateRowsCalls = [];
  for (const productType of _availabilities.config.productTypes) {
    evaluateRowsCalls.push(evaluateRows(page, currency, productType));
  }
  await Promise.all(evaluateRowsCalls);
  await page.close();
}

async function evaluateRows(page: puppeteer.Page, currency: string, productType: string): Promise<void> {
  const rows = await page.$x(`//tr[contains(., '${productType}')]`); // ROW SELECTOR
  const evaluateCellsCalls = [];
  for (const row of rows) {
    evaluateCellsCalls.push(evaluateCells(row, currency, productType));
  }
  await Promise.all(evaluateCellsCalls);
}

async function evaluateCells(row: puppeteer.ElementHandle, currency: string, productType: string): Promise<void> {
  const rawCells = await row.asElement().$$eval('td', cells => cells.map(cell => cell.innerText)); // CELL SELECTOR
  _availabilities[currency][_.camelCase(productType)].push({
    APY: rawCells[2],
    days: rawCells[3],
  });
}

function clearAvailabilities(currency: string): void {
  _availabilities[currency].fixedSavings = [];
  _availabilities[currency].lockedStaking = [];
}

function cleanEmptyProducts(): void {
  _.entries(_.omit(_availabilities, 'config')).map((entries) => {
    if (entries[1].fixedSavings.length === 0) {
      _.unset(_availabilities, [entries[0], 'fixedSavings'])
    }
    if (entries[1].lockedStaking.length === 0) { _.unset(_availabilities, [entries[0], 'lockedStaking']) }
  });
}

export async function scrape(): Promise<void> {
  console.log('Scraping....');
  await setUp();

  const currencies = _.tail(_.keys(_availabilities));

  const crawlPageCalls = [];
  for (const currency of currencies) {
    clearAvailabilities(currency);
    crawlPageCalls.push(crawlPage(currency));
  }
  await Promise.all(crawlPageCalls);
  _availabilities.config.cache.lastUpdated = Date.now();
  cleanEmptyProducts();
  writeFile(path.join(__dirname, '/availabilities.json'), JSON.stringify(_availabilities), 'utf-8', function (err) {
    if (err) {
        console.log("An error occured while writing JSON Object to File.");
        return console.log(err);
    }
 
    console.log("JSON file has been saved.");
  });
  console.log('Finished Scraping.');
}
