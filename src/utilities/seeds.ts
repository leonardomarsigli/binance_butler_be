import { AppDataSource } from "../data-source";
import { SavingProduct } from "../entity/SavingProduct";
import { Platform } from "../entity/Platform";
import { ExchangeRate } from "../entity/ExchangeRate";
import { RiskProfile, Strategy } from "../entity/Strategy";

export async function clearDatabase() {
  await AppDataSource.dropDatabase()
  await AppDataSource.synchronize()
}

export async function seedDatabase() {
  await AppDataSource.manager.save(
    AppDataSource.manager.create(ExchangeRate, {
      coin1: 'EUR',
      coin2: 'USD',
      rate: 1.102,
    })
  )

  const binance = await AppDataSource.manager.save(
    AppDataSource.manager.create(Platform, {
      name: 'Binance',
      endpoint: 'https://binance.com/',
      fees: {
        withdrawalFee: {
          type: 'fixed',
          amount: 2,
        },
        tradingFee: {
          type: 'percentage',
          amount: 0.001,
        }
      }
    })
  )
  
  const anchor = await AppDataSource.manager.save(
    AppDataSource.manager.create(Platform, {
      name: 'Anchor Protocol',
      endpoint: 'https://app.anchorprotocol.com/',
      fees: {
        depositFee: {
          type: 'percentage',
          amount: 0.001,
        },
        withdrawalFee: {
          type: 'percentage',
          amount: 0.001,
        }
      }
    })
  )

  const flexibleBUSDTier1 = await AppDataSource.manager.save(
    AppDataSource.manager.create(SavingProduct, {
      available: true,
      platform: binance,
      type: 'Flexible',
      coin: 'BUSD',
      max: 500,
      APY: 0.1,
    })
  )
  const flexibleBUSDTier2 = await AppDataSource.manager.save(
    AppDataSource.manager.create(SavingProduct, {
      available: true,
      platform: binance,
      type: 'Flexible',
      coin: 'BUSD',
      min: 500,
      max: 20000,
      APY: 0.015,
    })
  )
  const flexibleBUSDTier3 = await AppDataSource.manager.save(
    AppDataSource.manager.create(SavingProduct, {
      available: true,
      platform: binance,
      type: 'Flexible',
      coin: 'BUSD',
      min: 20000,
      APY: 0.008,
    })
  )
  const fixedBUSDTier1 = await AppDataSource.manager.save(
    AppDataSource.manager.create(SavingProduct, {
      available: true,
      platform: binance,
      type: 'Fixed',
      coin: 'BUSD',
      lotSize: 100,
      days: 30,
      reward: 0.4109589,
      APY: 0.05,
    })
  )
  const fixedBUSDTier2 = await AppDataSource.manager.save(
    AppDataSource.manager.create(SavingProduct, {
      available: true,
      platform: binance,
      type: 'Fixed',
      coin: 'BUSD',
      lotSize: 100,
      days: 60,
      reward: 1.47945205,
      APY: 0.09,
    })
  )

  await AppDataSource.manager.save(
    AppDataSource.manager.create(SavingProduct, {
      available: true,
      platform: binance,
      type: 'Flexible',
      coin: 'USDT',
      max: 2000,
      APY: 0.1,
    })
  )
  await AppDataSource.manager.save(
    AppDataSource.manager.create(SavingProduct, {
      available: true,
      platform: binance,
      type: 'Flexible',
      coin: 'USDT',
      min: 2000,
      max: 75000,
      APY: 0.03
    })
  )
  await AppDataSource.manager.save(
    AppDataSource.manager.create(SavingProduct, {
      available: true,
      platform: binance,
      type: 'Flexible',
      coin: 'USDT',
      min: 75000,
      APY: 0.001
    })
  )
  await AppDataSource.manager.save(
    AppDataSource.manager.create(SavingProduct, {
      available: true,
      platform: binance,
      type: 'Fixed',
      coin: 'USDT',
      days: 30,
      lotSize: 100,
      reward: 0.4109589,
      APY: 0.05,
    })
  )
  await AppDataSource.manager.save(
    AppDataSource.manager.create(SavingProduct, {
      available: true,
      platform: binance,
      type: 'Fixed',
      coin: 'USDT',
      days: 60,
      lotSize: 100,
      reward: 1.47945205,
      APY: 0.09,
    })
  )

  const anchorEarn = await AppDataSource.manager.save(
    AppDataSource.manager.create(SavingProduct, {
      available: true,
      platform: anchor,
      type: 'Flexible',
      coin: 'UST',
      APY: 0.195,
    })
  )

  await AppDataSource.manager.save(
    AppDataSource.manager.create(ExchangeRate, {
      coin1: 'EUR',
      coin2: 'USD',
      rate: 1.102
    })
  )

  await AppDataSource.manager.save(
    AppDataSource.manager.create(Strategy, {
      name: 'Anchor Protocol Earn Strategy',
      riskProfile: RiskProfile.LOW,
      description: 'this is a description',
      investmentCoins: ['UST'],
      savingProducts: [anchorEarn],
    })
  )
  await AppDataSource.manager.save(
    AppDataSource.manager.create(Strategy, {
      name: 'BUSD Locked Strategy',
      riskProfile: RiskProfile.VERY_LOW,
      description: 'this is a description',
      investmentCoins: ['BUSD'],
      savingProducts: [fixedBUSDTier2, flexibleBUSDTier1, fixedBUSDTier1, flexibleBUSDTier2, flexibleBUSDTier3],
    })
  )
}
