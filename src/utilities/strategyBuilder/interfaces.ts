import { SavingProduct } from "../../entity/SavingProduct";
import { Strategy } from "../../entity/Strategy";

export interface FeeDetail {
  type: 'percentage' | 'fixed';
  amount: number;
}

export interface ApplicableFees {
  deposit?: FeeDetail,
  trade?: FeeDetail,
  transfer?: FeeDetail,
  withdrawal?: FeeDetail,
}

export interface CalculatedFees {
  deposit?: number,
  trade?: number,
  transfer?: number,
  withdrawal?: number,
  total?: number,
}

export interface Statistics {
  PNL: number,
  ROI: number,
  annualizedAPY: number,
}

export interface Cash {
  amount: number,
  currency: string,
}

export interface SetupInfo {
  strategy: Strategy,
  days: number,
  exchangeRate: number,
}

export interface CurrentInfo {
  day: number,
  amountLocked: number,
  amountAvailable: number,
  moves: Move[],
}

export interface AvailableSavingProduct {
  product: SavingProduct,
  availableAmount: number,
}

export type FeeType = 'deposit' | 'trade' | 'transfer' | 'withdrawal';
export type Stage = 'opening' | 'closing';

export interface Principal {
  principalBeforeFees: number,
  principalAfterFees: number,
  applicableFees?: ApplicableFees,
  calculatedFees?: CalculatedFees,
}

export interface Financials {
  exchangeRate?: number,
  opening: Principal,
  closing: Principal,
  statistics: Statistics,
}

export interface Move {
  platform: string,
  startDay: number,
  endDay: number,
  product: SavingProduct,
  amount: number,
  interest: number,
}

export interface IProposedStrategy {
  strategy: Strategy,
  moves: Move[]
  financials: Financials,
}