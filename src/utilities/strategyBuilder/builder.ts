import _ = require("lodash");
import { ExchangeRate } from "../../entity/ExchangeRate";
import { SavingProduct } from "../../entity/SavingProduct";
import { Strategy } from "../../entity/Strategy";
import { AvailableSavingProduct, Cash, CurrentInfo, FeeType, IProposedStrategy, Move, Principal, SetupInfo, Stage, Statistics } from "./interfaces";

export interface StrategyBuilderProps {
  amount: number,
  days: number,
  strategy: Strategy,
  currency: 'EUR' | 'USD',
}

export class StrategyBuilder {
  initialAmount: Cash;
  setupInfo: SetupInfo;
  orderedProducts: SavingProduct[];
  current: CurrentInfo;
  opening: Principal;
  closing: Principal;
  movesHistory: Move[];
  statistics: Statistics;

  constructor({ amount, days, strategy, currency }: StrategyBuilderProps) {
    this.initialAmount = {
      amount,
      currency,
    };
    this.setupInfo = {
      strategy,
      days,
      exchangeRate: 1,
    };
    this.current = {
      day: 0,
      amountLocked: 0,
      amountAvailable: 0,
      moves: [],
    };
    this.opening = {
      principalBeforeFees: amount,
      principalAfterFees: amount,
      applicableFees: {},
      calculatedFees: {},
    };
    this.closing = {
      principalBeforeFees: 0,
      principalAfterFees: 0,
      applicableFees: {},
      calculatedFees: {},
    };
    this.statistics = {
      PNL: 0,
      ROI: 0,
      annualizedAPY: 0,
    };
    this.movesHistory = [];
    
  }

  async run() {
    await this.completeSetup();
    await this.simulate();
    this.calculateProfits();
    this.calculateFees('closing');
    this.calculateStatistics();
  }

  async calculateStrategy(): Promise<IProposedStrategy> {
    await this.run();
    return {
      strategy: this.setupInfo.strategy,
      moves: this.movesHistory,
      financials: {
        exchangeRate: this.setupInfo.exchangeRate,
        opening: this.opening,
        closing: this.closing,
        statistics: this.statistics,
      }
    }
  }

  async completeSetup() {
    this.orderedProducts = await this.setupInfo.strategy.orderedProducts();
    if (this.initialAmount.currency !== 'USD') await this.retrieveExchangeRate();
    await this.retrieveFees();
    this.calculateFees('opening');
    this.closing.principalBeforeFees = this.opening.principalAfterFees;
    this.current.amountAvailable = this.opening.principalAfterFees;
  }

  async retrieveExchangeRate() {
    this.setupInfo.exchangeRate = (await ExchangeRate.findOneBy({ coin1: this.initialAmount.currency, coin2: 'USD'})).rate
  };

  async retrieveFees() {
    const { openingFees, closingFees } = await this.setupInfo.strategy.fees();
    this.opening.applicableFees = openingFees;
    this.closing.applicableFees = closingFees;
  };

  calculateFees(stage: Stage): void {
    this.calculateFee(stage, 'deposit');
    if (stage === 'opening') this.exchangeCurrency(stage);

    this.calculateFee(stage, 'trade');
    this.calculateFee(stage, 'transfer');

    if (stage === 'closing') this.exchangeCurrency(stage);

    this.calculateFee(stage, 'withdrawal');
    this[stage].calculatedFees.total = _.sum(Object.values(this[stage].calculatedFees));
  }

  

  calculateFee(stage: Stage, type: FeeType): void {
    const feeDetail = this[stage].applicableFees[type];
    let calculatedFee: number;

    if (!feeDetail) return;
    if (feeDetail.type === 'fixed') calculatedFee = feeDetail.amount;
    if (feeDetail.type === 'percentage') calculatedFee = feeDetail.amount * this[stage].principalAfterFees;

    this[stage].calculatedFees[type] = calculatedFee;
    this[stage].principalAfterFees -= calculatedFee;
  }

  exchangeCurrency(stage: Stage): void {
    let operator: 'divide' | 'multiply';
    operator = stage === 'opening' ? 'multiply' : 'divide';
    this[stage].principalAfterFees = _[operator](this[stage].principalAfterFees, this.setupInfo.exchangeRate);
  }

  async simulate(): Promise<void> {
    this.redeemMoves();
    if (this.current.amountAvailable > 0) {
      const availableProducts = this.availableProducts();
      await this.evaluateProducts(availableProducts);
    }
    this.current.day += 1;
    if (this.current.day < this.setupInfo.days) await this.simulate();
  }

  redeemMoves(): void {
    const unlockedMoves = _.filter(this.current.moves, (move) => {
      return move.endDay === this.current.day
    });
    _.pull(this.current.moves, ...unlockedMoves);
    const unlockedCash = _.sumBy(unlockedMoves, (move) =>  {
      return (move.amount);
    });

    this.current.amountAvailable += _.sumBy(unlockedMoves, (move) => {
      return move.amount + move.interest;
    });
    this.current.amountLocked -= _.sumBy(unlockedMoves, (move) => {
      return move.amount;
    });
  }

  availableProducts(): AvailableSavingProduct[] {
    const availableProducts: AvailableSavingProduct[] = [];
    for (const product of this.orderedProducts) {
      const availableAmount = this.availableAmount(product);
      if (availableAmount > 0) {
        availableProducts.push({
          product,
          availableAmount,
        })
      }
    }
    return availableProducts;
  }

  availableAmount(product: SavingProduct): number {
    if (!product.max) return 1000000000;
    if (!product.isAvailable()) return 0;

    const relatedMoves = _.filter(this.current.moves, (move) => {
      return move.product === product
    });
    return product.max - _.sumBy(relatedMoves, (move) => {
      return move.amount;
    });
  }

  async evaluateProducts(availableProducts: AvailableSavingProduct[]): Promise<void> {
    for (const availableProduct of availableProducts) {
      if (availableProduct.product.isFlexible()) {
        await this.evaluateFlexibleProduct(availableProduct);
      } else {
        await this.evaluateFixedProduct(availableProduct);
      }
    }
  }

  async evaluateFlexibleProduct({ availableAmount, product }: AvailableSavingProduct) {
    const amountUsed = _.min([availableAmount, this.current.amountAvailable]);
    const move: Move = {
      platform: (await product.findPlatform()).name,
      startDay: this.current.day,
      endDay: this.setupInfo.days,
      product,
      amount: amountUsed,
      interest: amountUsed * (1 + product.dailyAPY())**(this.setupInfo.days - this.current.day) - amountUsed,
    };

    this.updateMoves(move);
  }

  async evaluateFixedProduct({ availableAmount, product }: AvailableSavingProduct): Promise<void> {
    if (this.current.day + product.days > this.setupInfo.days) return;

    const lots = Math.floor(this.current.amountAvailable / product.lotSize);
    const move: Move = {
      platform: (await product.findPlatform()).name,
      startDay: this.current.day,
      endDay: this.current.day + product.days,
      product,
      amount: lots * product.lotSize,
      interest: lots * product.reward,
    };

    this.updateMoves(move);
  }

  updateMoves(move: Move): void {
    if (move.amount === 0) return;

    this.current.moves.push(move);
    this.movesHistory.push(move);
    this.current.amountLocked += move.amount;
    this.current.amountAvailable -= move.amount;
  }

  calculateProfits(): void {
    const profits = _.sum(this.movesHistory.map(move => (move.interest)));
    const resultingPrincipal = this.opening.principalAfterFees + profits;
    this.closing.principalBeforeFees = resultingPrincipal;
    this.closing.principalAfterFees = resultingPrincipal;
  }

  calculateStatistics(): void {
    this.statistics.PNL = _.round(this.closing.principalAfterFees - this.opening.principalBeforeFees, 1);
    this.statistics.ROI = _.round(this.statistics.PNL / this.opening.principalBeforeFees * 100, 1);
    this.statistics.annualizedAPY = _.round(this.statistics.ROI / this.setupInfo.days * 365, 1);
  }
}