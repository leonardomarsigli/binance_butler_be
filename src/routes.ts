import { StrategiesController } from "./controller/StrategiesController"
import { ProductsController } from "./controller/ProductsController"

export const Routes = [
    {
        method: "post",
        route: "/strategies/",
        controller: StrategiesController,
        action: "all"
    },
    {
        method: "post",
        route: "/products",
        controller: ProductsController,
        action: "all",
    }
]