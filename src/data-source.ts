import "reflect-metadata"
import { DataSource } from "typeorm"
import { SavingProduct } from "./entity/SavingProduct"
import { Coin } from "./entity/Coin"
import { Platform } from "./entity/Platform"
import { Strategy } from "./entity/Strategy"
import { ExchangeRate } from "./entity/ExchangeRate"

export const AppDataSource = new DataSource({
    type: "postgres",
    host: process.env.POSTGRES_HOST || "localhost",
    port: 5432,
    username: "postgres",
    password: process.env.POSTGRES_PASSWORD,
    database: "postgres",
    synchronize: true,
    logging: false,
    entities: [Platform, Coin, SavingProduct, Strategy, ExchangeRate],
    migrations: [],
    subscribers: [],
})
