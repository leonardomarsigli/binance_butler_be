import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne, ManyToMany } from "typeorm"
import { Platform } from "./Platform";
import { Strategy } from "./Strategy";

@Entity()
export class SavingProduct extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column({ nullable: false, enum: ['Fixed', 'Flexible'] })
    type: string

    @ManyToOne(() => Platform, platform => platform.savingProducts)
    platform: Platform

    @Column({ nullable: true })
    available: boolean

    @Column({ nullable: false })
    coin: string

    @Column({ nullable: true })
    min: number

    @Column({ nullable: true })
    max: number

    @Column({ nullable: true })
    days: number

    @Column({ nullable: true })
    lotSize: number

    @Column({ type: 'float', nullable: true })
    reward: number

    @Column({ type: 'float', nullable: false })
    APY: number;

    @ManyToMany(() => Strategy, strategy => strategy.savingProducts)
    strategies: Strategy[]

    isFlexible() {
      return this.type === 'Flexible';
    }

    dailyAPY() {
      return this.APY / 365;
    }

    isAvailable() {
      return this.available;
    }

    async findPlatform() {
      return (await SavingProduct.find({ relations: { platform: true }})).find((savingProduct) => {
        return savingProduct.id === this.id
      }).platform;
    }
}
