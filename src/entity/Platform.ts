import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany } from "typeorm"
import { SavingProduct } from "./SavingProduct";

interface FeeDetail {
  type: 'percentage' | 'fixed';
  amount: number;
}

@Entity()
export class Platform extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column({ nullable: false })
    name: string

    @Column({ nullable: true })
    endpoint: string

    @Column('json', { nullable: true })
    fees: {
      depositFee?: FeeDetail,
      withdrawalFee?: FeeDetail,
      tradingFee?: FeeDetail,
    }

    @OneToMany(() => SavingProduct, savingProduct => savingProduct.platform)
    savingProducts: SavingProduct[]

    async findSavingProducts() {
      return (await Platform.find({ relations: { savingProducts: true }})).find((platform) => {
        return platform.id === this.id
      }).savingProducts;
    }
}
