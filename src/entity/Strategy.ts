import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany, ManyToMany, JoinTable } from "typeorm" 
import { Platform } from "./Platform";
import { SavingProduct } from "./SavingProduct";
import _ = require('lodash');
import { ApplicableFees } from "../utilities/strategyBuilder/interfaces";

export enum RiskProfile {
  VERY_LOW = 0,
  LOW = 1,
  LOW_MEDIUM = 2,
  MEDIUM = 3,
  MEDIUM_HIGH = 4,
  HIGH = 5,
  VERY_HIGH = 6,
}
@Entity()
export class Strategy extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column({ nullable: false })
  name: string

  @Column({ nullable: true })
  description: string

  @Column({
    type: 'enum',
    
    enum: RiskProfile,
    nullable: false,
  })
  riskProfile: RiskProfile

  @Column('simple-array', { nullable: false })
  investmentCoins: string[]

  @ManyToMany(() => SavingProduct, savingProduct => savingProduct.strategies, { cascade: true })
  @JoinTable()
  savingProducts: SavingProduct[]

  async binance() {
    return await Platform.findOneBy({ name: 'Binance' });
  }

  async fees(): Promise<{ openingFees: ApplicableFees; closingFees: ApplicableFees; }> {
    const binance = await this.binance();
    switch (this.name) {
      case 'Anchor Protocol Earn Strategy':
        const anchor = await Platform.findOneBy({ name: 'Anchor Protocol'});
        return {
          openingFees: {
            deposit: binance.fees.depositFee,
            trade: binance.fees.tradingFee,
            transfer: anchor.fees.depositFee,
          },
          closingFees: {
            transfer: anchor.fees.withdrawalFee,
            trade: binance.fees.tradingFee,
            withdrawal: binance.fees.withdrawalFee,
          }
        }
      default:
        return {
          openingFees: {
            trade: binance.fees.tradingFee,
          },
          closingFees: {
            trade: binance.fees.tradingFee,
          }
        }
    }
  }

  async orderedProducts(): Promise<SavingProduct[]> {
    const savingProducts = (await Strategy.find({ relations: { savingProducts: true }})).find((strategy) => {
      return strategy.id === this.id
    }).savingProducts;
    return _.reverse(_.sortBy(savingProducts, (product) => {
      return product.APY
    }));
  }
}
