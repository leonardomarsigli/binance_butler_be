import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne } from "typeorm"

@Entity()
export class ExchangeRate extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column({ nullable: false })
    coin1: string

    @Column({ nullable: false })
    coin2: string

    @Column({ type: 'float', nullable: false })
    rate: number
}
