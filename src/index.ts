import * as express from "express"
import * as bodyParser from "body-parser"
import { Request, Response } from "express"
import { AppDataSource } from "./data-source"
import { Routes } from "./routes"
import { clearDatabase, seedDatabase } from "./utilities/seeds"
import { scrape } from "./utilities/scraper/script"

function handleError(err, res) {
    res.status(res.statusCode || 500).send({ message: err.message })
}

AppDataSource.initialize().then(async () => {
    const app = express()
    const port = process.env.PORT || 3000;

    app.use(bodyParser.json())

    Routes.forEach(route => {
        (app as any)[route.method](route.route, async (req: Request, res: Response, next: Function) => {
            try {
                const result = await (new (route.controller as any))[route.action](req, res, next)
                res.json(result)
            } catch(error) {
                next(error)
            }
        })
    })

    scrape();
    setInterval(scrape, 1000 * 60 * 10);
    // await clearDatabase()
    // await seedDatabase()

    app.use(handleError)
    app.listen(port, () => console.log(`Express server has started on port ${port}, http://localhost:3000`))

}).catch(error => console.log(error))
